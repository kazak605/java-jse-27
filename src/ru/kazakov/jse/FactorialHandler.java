package ru.kazakov.jse;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

public class FactorialHandler implements InvocationHandler {

    Factorial factorial;
    Map<Integer, BigInteger> factorialCache = new LinkedHashMap<>();
    Logger logger = Logger.getLogger(FactorialHandler.class.getName());

    public FactorialHandler(Factorial factorial) {
        this.factorial = factorial;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals("getFactorial")) {
            Integer number = (Integer) args[0];
            if (factorialCache.get(args[0]) != null) {
                logger.info("Factorial of a number from the cache");
                return factorialCache.get(args[0]);
            }
            BigInteger value = (BigInteger) method.invoke(factorial, args);
            factorialCache.put(number, value);
            return value;
        }
        return method.invoke(factorial, args);
    }
}
