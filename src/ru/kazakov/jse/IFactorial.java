package ru.kazakov.jse;

import java.math.BigInteger;

public interface IFactorial {

    BigInteger getFactorial(Integer arg);

}
