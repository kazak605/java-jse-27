package ru.kazakov.jse;

import java.math.BigInteger;

public class Factorial implements IFactorial{

    @Override
    public BigInteger getFactorial(Integer arg) {
        if (arg == 0) {
            return BigInteger.ONE;
        }
        BigInteger result = new BigInteger(String.valueOf(1));
        for (int i = 1; i <= arg; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }
}
