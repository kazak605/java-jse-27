package ru.kazakov.jse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Proxy;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws IOException {

        Factorial factorial = new Factorial();
        FactorialHandler factorialHandler = new FactorialHandler(factorial);
        IFactorial proxy = (IFactorial) Proxy.newProxyInstance(factorial.getClass().getClassLoader(), factorial.getClass().getInterfaces(), factorialHandler);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("Enter an integer:");
            String value = bufferedReader.readLine();
            if (value.equals("exit")) {
                logger.log(Level.INFO, "Program exit");
                break;
            }
            Integer intValue = Integer.parseInt(value);
            if (intValue < 0) {
                logger.log(Level.WARNING, "Error! The number is negative.");
            }
            System.out.println(proxy.getFactorial(intValue));
        }
    }

}
